import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home/Home.vue'
import Tombolo from '../components/Home/brand/tombolo.vue'
import Degree137 from '../components/Home/brand/137.vue'
import Malee from '../components/Home/brand/malee.vue'
import Danielle from '../components/Home/brand/danielle.vue'
import Milky from '../components/Home/brand/milky.vue'
import Risotto from '../components/Home/brand/risotto.vue'
import Maretti from '../components/Home/brand/maretti.vue'
import Contact from '../components/Contact'
import Brands from '../components/Brands'
import Notice from '../components/Notice'

Vue.use(Router)

const routes = [
  { path: '/', component: Home },
  { path: '/tombolo/detail', component: Tombolo },
  { path: '/degree137/detail', component: Degree137 },
  { path: '/malee/detail', component: Malee },
  { path: '/danielle/detail', component: Danielle },
  { path: '/milky/detail', component: Milky },
  { path: '/risotto/detail', component: Risotto },
  { path: '/maretti/detail', component: Maretti },
  { path: '/contact', component: Contact },
  { path: '/brands', component: Brands },
  { path: '/notice', component: Notice }
]

export default new Router({
  routes
})
